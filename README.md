# Campus Map with uMap

[Link to uMap](http://umap.osm.ch/m/3313/)

## How to keep the uMap up to date

To update the floors simply go to [Overpass](https://overpass-turbo.osm.ch/) and make sure that the whole campus is covered in the map to the right.

Insert the Query
```
[out:json][timeout:25];
(
  nwr["level"="0"]({{bbox}});
);
out body;
>;
out skel qt;
```
---
**NOTE:**

Change the number next to level to the respective number of the floor you want to update

---

Next, export the result as a geojson file.
On Umap go to the "Import data" tab (Ctrl + I), select the geojson file, select the correct layer,
don't forget to check "Replace Layer content"  and then hit "Import".
